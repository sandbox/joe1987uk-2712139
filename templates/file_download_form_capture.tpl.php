<?php

/**
 * @file file_download_form_capture.tpl.php
 * Template implementation to display file download with form capture popup.
 *
 */
?>
<div class="popup <?php print $classes; ?>"<?php print $attributes; ?>>

  <a target="_blank" class="ctools-use-modal" 
      href="/download/file/nojs/<?php print $item['#file']['fid']; ?>/<?php print $item['#form_capture']; ?>">
    <?php if(empty($item['#file']['description'])): ?>
        <?php print $item['#file']['filename']; ?>
    <?php else: ?>
      <?php print $item['#file']['description']; ?>
    <?php endif; ?>
  </a>

</div>
